alias add-lag='sudo tc qdisc add dev lo root handle 1:0 netem delay 600msec'
alias remove-lag='sudo tc qdisc del dev lo root'

alias gif='~/vidToGif'

alias find='sudo find / -name'
alias grep='grep --color=auto -E'

alias ifconfig=/sbin/ifconfig
 
# some more ls aliases
alias ll='ls --color=auto -halp'
alias la='ls --color=auto -A'
alias l='ls --color=auto -CF'
